import React from "react";

const Moviescard = (handlewatchmovies) => {
  return (
    <>
      <div className="dropdown">
        <select
          onClick={(e) =>
            handlewatchmovies(e.target.value)
          }
        >
          <option value={"currwatch"}> curr watch</option>
          <option value={"willwatch"}> will watch</option>
          <option value={"wantwatch"}> want watch</option>
        </select>
      </div>
    </>
  );
};

export default Moviescard;
