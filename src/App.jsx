import { useState } from "react";
import "./App.css";
import data1 from "./assests/api";
import Moviescard from "./Moviescard";

function App() {
  const [movies, setMovies] = useState(data1);
  console.log(movies);

  function handlemovies(name, rank, currstatus) {
    console.log(name, rank);

    const newmovies = movies.map((item) => {
      if (item.Rank == rank) {
        return { ...item, [name]: "true", [currstatus]: "false" };
      }
      return item;
    });
    setMovies(newmovies);
  }

  return (
    <>
      <h1>currently watching</h1>
      <div className="currmovies">
        {movies
          .filter((item) => {
            return item.currwatch == "true";
          })
          .map((item) => {
            return (
              <div>
                {item.Title}
                  
                <Moviescard  handlewatchmovies={handlemovies} />
              </div>
            );
          })}
      </div>

      <h1>wanting to watch</h1>
      <div className="wantingmovies">
        {movies
          .filter((item) => {
            return item.wantwatch == "true";
          })
          .map((item) => {
            return <div>{item.Title}
              <Moviescard handlewatchmovies={handlemovies}/>
            </div>;
          })}
      </div>
      <h1>will watch</h1>
      <div className="willmovies">
        {movies
          .filter((item) => {
            return item.willwatch == "true";
          })
          .map((item) => {
            return (
              <div>
                {item.Title}
                <Moviescard handlewatchmovies={handlemovies}/>
              </div>
            );
          })}
      </div>
    </>
  );
}

export default App;
